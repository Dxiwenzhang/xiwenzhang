package com.xiwen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
/*@MapperScan(basePackages = "com.xiwen.xiwenzhang")//建议直接在dao上加@Mapper*/
public class XiwenzhangApplication {

	public static void main(String[] args) {
		SpringApplication.run(XiwenzhangApplication.class, args);
	}

}
