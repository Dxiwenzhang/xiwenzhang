package com.xiwen.common.baseentity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Dep 返回实体封装
 * @Author xiwen
 * @Date 2021/10/28 21:12
 */
@Data
@AllArgsConstructor
public class ResponsEntity<T> {

    private Integer status;
    private String msg;
    private T data;

    public ResponsEntity(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }

}
