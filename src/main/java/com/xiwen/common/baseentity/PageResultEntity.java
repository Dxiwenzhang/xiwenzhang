package com.xiwen.common.baseentity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @Dep 分页返回实体封装
 * @Author xiwen
 * @Date 2021/10/8 13:54
 */
@Data
@AllArgsConstructor
public class PageResultEntity {
    private List rows;
    private Long total;

}
