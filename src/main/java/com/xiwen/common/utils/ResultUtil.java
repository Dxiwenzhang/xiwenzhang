package com.xiwen.common.utils;

import com.xiwen.common.baseentity.PageResultEntity;
import com.xiwen.common.baseentity.ResponsEntity;
import lombok.experimental.UtilityClass;

import java.util.List;

/**
 * @Dep 对分页结果进行封装
 * @Author xiwen
 * @Date 2021/11/3 21:44
 */
@UtilityClass
public class ResultUtil {
    
    public static ResponsEntity success() {
        return vo(CommonConst.SUCCESS_CODE, null, null);
    }

    public static ResponsEntity success(String msg) {
        return vo(CommonConst.SUCCESS_CODE, msg, null);
    }

    public static ResponsEntity success(String msg, Object data) {
        return vo(CommonConst.SUCCESS_CODE, msg, data);
    }

    public static ResponsEntity error() {
        return vo(CommonConst.FAIL_CODE, null, null);
    }

    public static ResponsEntity error(String msg) {
        return vo(CommonConst.FAIL_CODE, msg, null);
    }

    public static ResponsEntity error(String msg, Object data) {
        return vo(CommonConst.FAIL_CODE, msg, data);
    }

    public static PageResultEntity result(List<?> list, Long total) {
        return new PageResultEntity(list, total);
    }

    public static ResponsEntity vo(Integer status, String message, Object data) {
        return new ResponsEntity<>(status, message, data);
    }


}
