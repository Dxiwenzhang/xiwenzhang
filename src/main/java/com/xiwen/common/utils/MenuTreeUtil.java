package com.xiwen.common.utils;

import com.xiwen.xiwenzhang.permission.entity.SysPermissionEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: MenuTreeUtil
 * @Author: xiwen
 * @Description: 返回树形工具类
 * @Date: 2021/10/30 15:57
 */

public class MenuTreeUtil {
    public List<SysPermissionEntity> menuCommon;

    public List<SysPermissionEntity> menuList(List<SysPermissionEntity> menu){ //controller层调用的方法   ，并将数据以list的形式返回
        this.menuCommon = menu;
        List<SysPermissionEntity> list = new ArrayList<SysPermissionEntity>();
        for (SysPermissionEntity x : menu) {
            SysPermissionEntity sysPermissionEntity = new SysPermissionEntity();
            if(x.getParentId().equals("0")){
                sysPermissionEntity.setId(x.getId());
                sysPermissionEntity.setPermissionId(x.getId());
                sysPermissionEntity.setTitle(x.getTitle());
                sysPermissionEntity.setUrl(x.getUrl());
                sysPermissionEntity.setIcon(x.getIcon());
                sysPermissionEntity.setChecked(x.isChecked());
                sysPermissionEntity.setParentId(x.getParentId());
                sysPermissionEntity.setChildren(menuChild(x.getId()));
                list.add(sysPermissionEntity);
            }
        }
        return list;
    }

    public List<SysPermissionEntity> menuChild(String id){ //子集查找遍历
        List<SysPermissionEntity> lists = new ArrayList<SysPermissionEntity>();
        for(SysPermissionEntity a : menuCommon){
            SysPermissionEntity sysPermissionEntity = new SysPermissionEntity();
            if(a.getParentId().equals(id)){
                sysPermissionEntity.setId(a.getId());
                sysPermissionEntity.setTitle(a.getTitle());
                sysPermissionEntity.setChecked(a.isChecked());
                sysPermissionEntity.setUrl(a.getUrl());
                sysPermissionEntity.setIcon(a.getIcon());
                sysPermissionEntity.setParentId(a.getParentId());
                sysPermissionEntity.setChildren(menuChild(a.getId()));
                lists.add(sysPermissionEntity);
            }
        }
        return lists;
    }

}