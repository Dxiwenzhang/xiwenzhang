package com.xiwen.common.utils;

import lombok.experimental.UtilityClass;

/**
 * @Dep 静态常量
 * @Author xiwen
 * @Date 2021/10/8 13:52
 */
@UtilityClass
public class CommonConst {

    public static final Integer SUCCESS_CODE = 200;
    public static final Integer FAIL_CODE = 500;

    // 1:全部资源，2：菜单资源
    public static final String[] MENU_FLAG = {"1", "2"};

    // 1:系统管理员
    public static final String ADMIN_ROLE ="1";

    public static final String TOP_MENU_ID = "0";
    public static final String TOP_MENU_NAME = "顶层菜单";

}
