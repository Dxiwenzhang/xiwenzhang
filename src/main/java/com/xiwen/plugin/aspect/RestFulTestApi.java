package com.xiwen.plugin.aspect;

import com.xiwen.plugin.redis.RedisUtil;
import com.xiwen.common.SpringContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


@RestController
@RequestMapping("/returnResult")
public class RestFulTestApi {

    @Autowired
    private   RedisUtil redisUtil;

    private Lock lock=new ReentrantLock();
    //设置价格
    @GetMapping("/setPrice")
    public String  setPrice(){
        redisUtil.set("price",10000);
        return "设置价格成功";
    }


    //获取价格--synchronized
    @GetMapping("/getPrice")
    public synchronized String  getPrice(){
        Object price = redisUtil.get("price");
        int i = Integer.parseInt(price.toString())-1;
        System.out.println(i);
        redisUtil.set("price",i);
        return "设置价格成功+"+i+"";
    }


    @GetMapping("/getLockPrice")
    public  String  getLockPrice(){
        long start = System.currentTimeMillis();
        System.out.println(start);
        int i = 0;
        try {
            lock.lock();
            Object price = redisUtil.get("price");
            i = Integer.parseInt(price.toString())-1;
            System.out.println(i);
            redisUtil.set("price",i);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
        long start2 = System.currentTimeMillis();
        System.out.println(start2);
        return "设置价格成功+"+i+"";
    }


    @GetMapping("/getThread")
    public  Object  getThread(){
        long start = System.currentTimeMillis();
//        System.out.println(start);
        Aaw bean = SpringContextHolder.getBean(Aaw.class);
        ThreadPoolManager threadPoolManager = ThreadPoolManager.getmInstance();
        Object submit = threadPoolManager.submit(bean);

        long start2 = System.currentTimeMillis();
        System.out.println(submit);
        return submit;
    }

    @Component
    class Aaw implements Callable<Object> {

        @Override
        public Object call() throws Exception {
            Object price = redisUtil.get("price");
            int j = Integer.parseInt(price.toString())-1;

            redisUtil.set("price",j);

            return  j;
        }
    }



    public static void main(String[] args){
        int m=1000;
         File src = new File("G:/zfsoa/zfsoa.sql");
        if(src.isFile()) {
            //获取文件的总长度
            long l = src.length();
            //获取文件名
            String fileName = src.getName().substring(0, src.getName().indexOf("."));
            //获取文件后缀
            String endName = src.getName().substring(src.getName().lastIndexOf("."));
            System.out.println(endName);
            InputStream in = null;
            try {
                in = new FileInputStream(src);
                for(int i = 1; i <= m; i++) {
                    StringBuffer sb = new StringBuffer();
                    sb.append(src.getParent()).append("\\").append(fileName)
                            .append("_data").append(i).append(endName);
                    System.out.println(sb.toString());
                    File file2 = new File(sb.toString());
                    //创建写文件的输出流
                    OutputStream out = new FileOutputStream(file2);
                    int len = -1;
                    byte[] bytes = new byte[10*1024*1024];
                    while((len = in.read(bytes))!=-1) {
                        out.write(bytes, 0, len);
                        if(file2.length() > (l / m)) {
                            break;
                        }
                    }
                    out.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
