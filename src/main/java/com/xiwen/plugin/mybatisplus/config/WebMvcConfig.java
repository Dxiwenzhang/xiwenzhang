package com.xiwen.plugin.mybatisplus.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;




@SpringBootConfiguration
public class WebMvcConfig extends WebMvcConfigurationSupport {


    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        //        静态资源映射
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }

}
