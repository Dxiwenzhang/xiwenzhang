package com.xiwen.xiwenzhang.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiwen.xiwenzhang.user.entity.SysUserRoleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 *  Mapper 接口
 * @author Xiwen
 * @since 2021-10-05
 */
@Mapper
public interface SysUserRoleDao extends BaseMapper<SysUserRoleEntity> {


}
