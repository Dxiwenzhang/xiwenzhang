package com.xiwen.xiwenzhang.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiwen.xiwenzhang.user.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *  Mapper 接口
 * @author Xiwen
 * @since 2021-10-05
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {

    List<SysUserEntity> getUserByName(@Param("username") String username);

    Page<SysUserEntity> selectUserList(@Param("page") Page<SysUserEntity> page, @Param("user") SysUserEntity user);

    List<SysUserEntity> findUserByRoleId(@Param("roleId") String roleId);
}
