package com.xiwen.xiwenzhang.user.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiwen.common.baseentity.PageResultEntity;
import com.xiwen.common.baseentity.ResponsEntity;
import com.xiwen.common.utils.ResultUtil;
import com.xiwen.plugin.mybatisplus.generator.util.UtilsConst;
import com.xiwen.plugin.mybatisplus.pageinfo.PageInfo;
import com.xiwen.xiwenzhang.permission.Service.SysPermissionService;
import com.xiwen.xiwenzhang.user.entity.SysUserEntity;
import com.xiwen.xiwenzhang.user.service.LoginService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Dep 用户控制器
 * @Author xiwen
 * @Date 2021/10/28 21:14
 */
@Controller
@Slf4j
@AllArgsConstructor
public class UserController {

    @Autowired
    private  SysPermissionService sysPermissionService;
    @Autowired
    private LoginService loginService;
    /**
     * @Dep 跳转至用户列表
     * @Author xiwen
     * @Date 2021/11/30 17:16
     */
    @GetMapping("/jumpUserList")
    public String jumpUserList(SysUserEntity user, Model model) {
        try {
            return UtilsConst.ADMIN_PREFIX +"user/user-list";
        } catch (Exception e) {
            log.error(String.format("RoleController.loadRoles%s", e));
            throw e;
        }
    }

    /**
     * @Dep 获取用户列表数据
     * @Author xiwen
     * @Date 2021/11/30 17:17
     */
    @ResponseBody
    @PostMapping("/getUserList")
    public PageResultEntity getUserList(SysUserEntity user) {
        try {
            Integer current = Integer.parseInt(user.getCurrent());
            Integer limit = Integer.parseInt(user.getLimit());
            Page<SysUserEntity> page = new PageInfo<>(current, limit);
            Page<SysUserEntity> userPage = loginService.selectUserList(user, page);
            PageResultEntity resultPage = ResultUtil.result(userPage.getRecords(), userPage.getTotal());
            return resultPage;
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * @Dep 新增用户
     * @Author xiwen
     * @Date 2021/11/27 19:11
     */
    @ResponseBody
    @PostMapping("/saveOrUpdateUser")
    public ResponsEntity saveOrUpdateUser(SysUserEntity user) {
        try {
            boolean b = loginService.saveOrUpdate(user);
            if (b) {
                //shiroService.updatePermission();
                return ResultUtil.success("添加更新成功");
            } else {
                return ResultUtil.error("添加更新失败");
            }
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * @Dep 跳转添加或编辑用户页面
     * @Author xiwen
     * @Date 2021/11/14 1:26
     */
    @GetMapping("/jumpAddOrUpdateUser")
    public String jumpAddOrUpdateUser(Model model,String id) {
        if (id!=null) {
            //编辑--查出数据再返回
            SysUserEntity user = loginService.getById(id);
            if (null != user) {
                model.addAttribute("user", user);
            }
        }
        //新增--直接返回页面
        return UtilsConst.ADMIN_PREFIX +"user/user-from";
    }

}
