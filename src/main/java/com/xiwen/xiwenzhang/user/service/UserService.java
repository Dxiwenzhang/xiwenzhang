package com.xiwen.xiwenzhang.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiwen.xiwenzhang.user.entity.SysUserEntity;

import java.util.List;


public interface UserService extends IService<SysUserEntity> {

    void addAssignRole(String userId, List<String> roleIdsList);
}
