package com.xiwen.xiwenzhang.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiwen.xiwenzhang.user.dao.SysUserDao;
import com.xiwen.xiwenzhang.user.entity.SysUserEntity;
import com.xiwen.xiwenzhang.user.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginServiceImpl extends ServiceImpl<SysUserDao, SysUserEntity> implements LoginService {
    @Autowired
    private SysUserDao sysUserDao;
    @Override
    public SysUserEntity getUserByName(String getMapByName) {
        List<SysUserEntity> userByNameList = sysUserDao.getUserByName(getMapByName);
        if(userByNameList.size()>0){
            return userByNameList.get(0);
        }
        return  null;
    }

    /**
     * @Dep 获取用户列表
     * @Author xiwen
     * @Date 2021/11/30 17:26
     */
    @Override
    public Page<SysUserEntity> selectUserList(SysUserEntity user, Page<SysUserEntity> page) {
        return sysUserDao.selectUserList(page, user);
    }


}
