package com.xiwen.xiwenzhang.user.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiwen.xiwenzhang.user.dao.SysUserDao;
import com.xiwen.xiwenzhang.user.dao.SysUserRoleDao;
import com.xiwen.xiwenzhang.user.entity.SysUserEntity;
import com.xiwen.xiwenzhang.user.entity.SysUserRoleEntity;
import com.xiwen.xiwenzhang.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl extends ServiceImpl<SysUserDao, SysUserEntity> implements UserService {
    @Autowired
    private SysUserRoleDao sysUserRoleDao;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addAssignRole(String userId, List<String> roleIdsList) {
        Map<String, Object> cloum = new HashMap<>();
        cloum.put("user_id",userId);
        sysUserRoleDao.deleteByMap(cloum);
        for (String roleId : roleIdsList) {
            SysUserRoleEntity userRole = new SysUserRoleEntity();
            userRole.setId(IdUtil.simpleUUID());
            userRole.setUserId(userId);
            userRole.setRoleId(roleId);
            userRole.setLayChecked(1);
            sysUserRoleDao.insert(userRole);
        }
    }
}
