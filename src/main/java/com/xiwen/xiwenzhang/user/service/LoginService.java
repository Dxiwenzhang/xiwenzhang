package com.xiwen.xiwenzhang.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiwen.xiwenzhang.user.entity.SysUserEntity;


public interface LoginService extends IService<SysUserEntity> {
    SysUserEntity getUserByName(String userName);

    Page<SysUserEntity> selectUserList(SysUserEntity user, Page<SysUserEntity> page);
}
