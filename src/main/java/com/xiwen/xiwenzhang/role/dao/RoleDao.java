package com.xiwen.xiwenzhang.role.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiwen.xiwenzhang.role.entity.SysRoleEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Dep 角色
 * @Author xiwen
 * @Date 2021/11/3 20:20
 */
@Mapper
public interface RoleDao extends BaseMapper<SysRoleEntity> {

    /**
     * 根据role参数查询角色列表
     * @param page
     * @param role role
     * @return list
     */
    Page<SysRoleEntity> selectRoleList(@Param("page") Page<SysRoleEntity> page, @Param("role") SysRoleEntity role);


    /**
     * @Dep 根据id查找
     * @Author xiwen
     * @Date 2021/11/27 1:08
     */
    SysRoleEntity findRoleById(@Param("id") String id);
}