package com.xiwen.xiwenzhang.role.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiwen.xiwenzhang.role.entity.SysRoleEntity;
import com.xiwen.xiwenzhang.user.entity.SysUserEntity;

import java.util.List;


public interface RoleService extends IService<SysRoleEntity> {

    /**
     * 根据条件查询角色列表
     * @param role
     * @return list
     */
    Page<SysRoleEntity> selectRoleList(SysRoleEntity role, Page<SysRoleEntity> page);

    /**
     * @Dep 根据id查找
     * @Author xiwen
     * @Date 2021/11/27 1:14
     */
    SysRoleEntity findRoleById(String id);

    /**
     * @Dep 分配权限
     * @Author xiwen
     * @Date 2021/12/6 22:13
     */
    void addAssignPermission(String roleId, List<String> permissionIdsList);

    /**
     * @Dep 查找该角色下面的所有用户
     * @Author xiwen
     * @Date 2021/12/6 22:42
     */
    List<SysUserEntity> findUserByRoleId(String roleId);
}
