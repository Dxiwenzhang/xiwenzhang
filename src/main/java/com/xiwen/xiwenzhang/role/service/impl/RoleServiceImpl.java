package com.xiwen.xiwenzhang.role.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiwen.xiwenzhang.permission.dao.SysRolePermissionDao;
import com.xiwen.xiwenzhang.permission.entity.SysRolePermissionEntity;
import com.xiwen.xiwenzhang.role.dao.RoleDao;
import com.xiwen.xiwenzhang.role.entity.SysRoleEntity;
import com.xiwen.xiwenzhang.role.service.RoleService;
import com.xiwen.xiwenzhang.user.dao.SysUserDao;
import com.xiwen.xiwenzhang.user.entity.SysUserEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Dep 服务实现类
 * @Author xiwen
 * @Date 2021/11/3 21:48
 */
@Service
@AllArgsConstructor
public class RoleServiceImpl extends ServiceImpl<RoleDao, SysRoleEntity> implements RoleService {

    private final RoleDao roleDao;

    private final SysRolePermissionDao sysRolePermissionDao;

    @Autowired
    private SysUserDao sysUserDao;

    @Override
    public Page<SysRoleEntity> selectRoleList(SysRoleEntity role, Page<SysRoleEntity> page) {
        return roleDao.selectRoleList(page, role);
    }

    @Override
    public SysRoleEntity findRoleById(String id) {
        SysRoleEntity sysRoleEntity = roleDao.findRoleById(id);
        return sysRoleEntity;
    }

    @Override
    public void addAssignPermission(String roleId, List<String> permissionIdsList) {
        Map<String, Object> cloum = new HashMap<>();
        cloum.put("role_id",roleId);
        sysRolePermissionDao.deleteByMap(cloum);
        for (String permissionId : permissionIdsList) {
            SysRolePermissionEntity rolePermission = new SysRolePermissionEntity();
            rolePermission.setId(IdUtil.simpleUUID());
            rolePermission.setPermissionId(permissionId);
            rolePermission.setRoleId(roleId);
            rolePermission.setChecked(true);
            sysRolePermissionDao.insert(rolePermission);
        }
    }

    @Override
    public List<SysUserEntity> findUserByRoleId(String roleId) {
        return sysUserDao.findUserByRoleId(roleId);
    }


}
