package com.xiwen.xiwenzhang.role.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xiwen.common.baseentity.BaseEntity;
import com.xiwen.xiwenzhang.user.entity.SysUserEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;


@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_role")
public class SysRoleEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -80449433292135181L;

    @TableId(type = IdType.UUID)
    @TableField("id")
    private String id;

    /**
     * 角色名称
     */
    @TableField("name")
    private String name;

    /**
     * 角色描述
     */
    @TableField("description")
    private String description;

    /**
     * 状态：1有效; 0无效
     */
    @TableField("status")
    private Integer status;


    /**
     * 选中状态---逻辑字段
     */
    @TableField("LAY_CHECKED")
    @JsonProperty("LAY_CHECKED")
    private boolean LAY_CHECKED;


    private SysUserEntity user;
}