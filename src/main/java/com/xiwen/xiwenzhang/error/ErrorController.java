package com.xiwen.xiwenzhang.error;

import com.xiwen.plugin.mybatisplus.generator.util.UtilsConst;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Dep 错误页面跳转
 * @Author xiwen
 * @Date 2021/10/31 17:31
 */
@Controller
@RequestMapping("/error")
@AllArgsConstructor
public class ErrorController {

    //错误页面跳转
    @GetMapping("/400")
    public String error() {
        String a="ss";
        return UtilsConst.ADMIN_PREFIX +"index/error";
    }

    @RequestMapping("/403")
    public String noPermission(HttpServletRequest request, HttpServletResponse response, Model model) {
        response.setStatus(HttpStatus.FORBIDDEN.value());
        return "error/403";
    }

    @RequestMapping("/404")
    public String notFund(HttpServletRequest request, HttpServletResponse response, Model model) {
        return "error/404";
    }

    @RequestMapping("/500")
    public String sysError(HttpServletRequest request, HttpServletResponse response, Model model) {
        return "error/500";
    }

}
