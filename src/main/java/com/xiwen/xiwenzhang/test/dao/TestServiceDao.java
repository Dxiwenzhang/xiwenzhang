package com.xiwen.xiwenzhang.test.dao;

import com.xiwen.xiwenzhang.test.entity.TestEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
/**
 * @Dep TestServiceDao
 * @Author Xiwen
 * @Date 2022-01-02
 */
@Mapper
public interface TestServiceDao extends BaseMapper<TestEntity> {

    Page<TestEntity> selectTestEntityList(@Param("page") Page<TestEntity> page, @Param("'test'") TestEntity test);
}
