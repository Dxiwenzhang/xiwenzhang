package com.xiwen.xiwenzhang.test.Service.impl;

import com.xiwen.xiwenzhang.test.Service.TestService;
import com.xiwen.xiwenzhang.test.entity.TestEntity;
import com.xiwen.xiwenzhang.test.dao.TestServiceDao;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class TestServiceImpl extends ServiceImpl<TestServiceDao, TestEntity> implements TestService {

    @Autowired
    private final TestServiceDao  testServiceDao;


    /**
     * @Dep 获取test列表
     * @Author Xiwen
     * @Date 2022-01-02
     */
    @Override
    public Page<TestEntity> selectTestEntityList(TestEntity test, Page<TestEntity> page) {
        return testServiceDao.selectTestEntityList(page, test);
    }


}
