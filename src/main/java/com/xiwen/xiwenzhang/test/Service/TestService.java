package com.xiwen.xiwenzhang.test.Service;

import com.xiwen.xiwenzhang.test.entity.TestEntity;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;


public interface TestService extends IService<TestEntity> {

    Page<TestEntity> selectTestEntityList(TestEntity test, Page<TestEntity> page);
}
