package com.xiwen.xiwenzhang.test.controller;

import com.xiwen.xiwenzhang.test.Service.TestService;
import com.xiwen.xiwenzhang.test.entity.TestEntity;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiwen.common.baseentity.PageResultEntity;
import com.xiwen.common.baseentity.ResponsEntity;
import com.xiwen.common.utils.ResultUtil;
import com.xiwen.plugin.mybatisplus.generator.util.UtilsConst;
import com.xiwen.plugin.mybatisplus.pageinfo.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Xiwen
 * @since 2022-01-02
 */
@Controller
@Slf4j
@AllArgsConstructor
public class TestController {
@Autowired
private TestService testService;

    /**
     * @Dep 跳转至test列表
     * @Author Xiwen
     * @Date 2022-01-02
     */
    @GetMapping("/jumpTestEntityList")
    public String jumpTestEntityList(TestEntity test, Model model) {
        try {
            return UtilsConst.ADMIN_PREFIX +"test/test-list";
        } catch (Exception e) {
            log.error(String.format("RoleController.loadRoles%s", e));
            throw e;
        }
    }

    /**
     * @Dep 获取test列表数据
     * @Author Xiwen
     * @Date 2022-01-02
     */
    @ResponseBody
    @PostMapping("/getTestEntityList")
    public PageResultEntity getTestEntityList(TestEntity test) {
        try {
            Integer current = Integer.parseInt(test.getCurrent());
            Integer limit = Integer.parseInt(test.getLimit());
            Page<TestEntity> page = new PageInfo<>(current, limit);
            Page<TestEntity> testPage = testService.selectTestEntityList(test, page);
            PageResultEntity resultPage = ResultUtil.result(testPage.getRecords(), testPage.getTotal());
            return resultPage;
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * @Dep 新增test
     * @Author Xiwen
     * @Date 2022-01-02
     */
    @ResponseBody
    @PostMapping("/saveOrUpdatetest")
    public ResponsEntity saveOrUpdatetest(TestEntity test) {
        try {
            boolean b = testService.saveOrUpdate(test);
            if (b) {
                //shiroService.updatePermission();
                return ResultUtil.success("添加更新成功");
            } else {
                return ResultUtil.error("添加更新失败");
            }
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * @Dep 跳转添加或编辑test页面
     * @Author Xiwen
     * @Date 2022-01-02
     */
    @GetMapping("/jumpAddOrUpdatetest")
    public String jumpAddOrUpdatetest(Model model,String id) {
        if (id!=null) {
            //编辑--查出数据再返回
            TestEntity test = testService.getById(id);
            if (null != test) {
                model.addAttribute("test", test);
            }
        }
        //新增--直接返回页面
        return UtilsConst.ADMIN_PREFIX +"test/test-from";
    }

}
