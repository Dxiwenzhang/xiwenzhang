package com.xiwen.xiwenzhang.permission.controller;


import com.xiwen.common.baseentity.ResponsEntity;
import com.xiwen.common.utils.CommonConst;
import com.xiwen.common.utils.ResultUtil;
import com.xiwen.plugin.mybatisplus.generator.util.UtilsConst;
import com.xiwen.xiwenzhang.permission.Service.SysPermissionService;
import com.xiwen.xiwenzhang.permission.entity.SysPermissionEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xiwen
 * @since 2021-10-29
 */
@Slf4j
@Controller
@AllArgsConstructor
public class SysPermissionController {

    @Autowired
    private final SysPermissionService permissionService;

    /**
     * @Dep 跳转权限菜单
     * @Author xiwen
     * @Date 2021/11/7 21:00
     */
    @GetMapping("/jumpMenuList")
    public String jumpMenuList() {
        return UtilsConst.ADMIN_PREFIX +"menu/menu-list";
    }



    /**
    * @Dep 权限菜单数据
    * @Author xiwen
    * @Date 2021/11/7 21:00
    */
    @PostMapping("/getMenuListData")
    @ResponseBody
    public List<SysPermissionEntity> getMenuListData(String flag) {
        List<SysPermissionEntity> menuList=null;
        try {
            if (StringUtils.isBlank(flag) || CommonConst.MENU_FLAG[0].equals(flag)) {
                menuList = permissionService.selectAll(null);
            } else if (CommonConst.MENU_FLAG[1].equals(flag)) {
                menuList = permissionService.selectPermissionList();
            }
        } catch (Exception e) {
            throw e;
        }
        return menuList;
    }

   /**
    * @Dep 跳转添加或编辑菜单页面
    * @Author xiwen
    * @Date 2021/11/14 1:26
    */
    @GetMapping("/JumpAddOrUpdateMenu")
    public String JumpAddMenu(Model model,String id) {
        if (id!=null) {
          //编辑--查出数据再返回
            SysPermissionEntity permission = permissionService.findPermissionById(id);
            if (null != permission) {
                if (CommonConst.TOP_MENU_ID.equals(permission.getParentId())) {
                    model.addAttribute("parentName", CommonConst.TOP_MENU_NAME);
                } else {
                    SysPermissionEntity parent = permissionService.findPermissionById(permission.getParentId());
                    if (parent != null) {
                        model.addAttribute("parentName", parent.getTitle());
                    }
                }
                model.addAttribute("permission", permission);
            }
        }
            //新增--直接返回页面
        return UtilsConst.ADMIN_PREFIX +"menu/menu-from";
    }

   /**
    * @Dep 添加或者更新菜单数据
    * @Author xiwen
    * @Date 2021/11/14 17:38
    */
    @ResponseBody
    @PostMapping("/saveOrUpdate")
    public ResponsEntity saveOrUpdate(SysPermissionEntity permission) {
        try {
            boolean b = permissionService.saveOrUpdate(permission);
            if (b) {
                //shiroService.updatePermission();
                return ResultUtil.success("添加权限成功");
            } else {
                return ResultUtil.error("添加权限失败");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * @Dep 删除菜单
     * @Author xiwen
     * @Date 2021/11/15 22:22
     */
    @ResponseBody
    @PostMapping("/deleteMenu")
    public ResponsEntity deleteMenu(String id) {
        try {
            int subPermsByPermissionIdCount = permissionService.selectPermissionById(id);
            if (subPermsByPermissionIdCount > 0) {
                return ResultUtil.error("改资源存在下级资源，无法删除！");
            }
            int a = permissionService.deleteById(id);
            if (a > 0) {
                //shiroService.updatePermission();
                return ResultUtil.success("删除权限成功");
            } else {
                return ResultUtil.error("删除权限失败");
            }
        } catch (Exception e) {
            throw e;
        }
    }
}

