package com.xiwen.xiwenzhang.permission.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiwen.xiwenzhang.permission.entity.SysRolePermissionEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 * @author Xiwen
 * @since 2021-10-29
 */
@Mapper
public interface SysRolePermissionDao extends BaseMapper<SysRolePermissionEntity> {


}
