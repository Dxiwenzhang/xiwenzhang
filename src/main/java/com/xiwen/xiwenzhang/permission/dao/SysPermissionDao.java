package com.xiwen.xiwenzhang.permission.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiwen.xiwenzhang.permission.entity.SysPermissionEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 * @author Xiwen
 * @since 2021-10-29
 */
@Mapper
public interface SysPermissionDao extends BaseMapper<SysPermissionEntity> {

    List<SysPermissionEntity> findMenuByUserId(@Param("userId") String userId);

    //自定义查询所有列表
    List<SysPermissionEntity> selectPermissionList();

    //查询所有
    List<SysPermissionEntity> selectAll(@Param("roleId") String roleId);

    //查询有没有子集，用于判断是否可以直接删除
    int selectPermissionById(String id);

    SysPermissionEntity findPermissionById(@Param("id") String id);
}
