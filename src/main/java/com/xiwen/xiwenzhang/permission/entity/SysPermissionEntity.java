package com.xiwen.xiwenzhang.permission.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author Xiwen
 * @since 2021-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_permission")
public class SysPermissionEntity implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(type = IdType.UUID)
    private String id;

    /**
     * 权限id
     */
    @TableField("permission_id")
    private String permissionId;

    /**
     * 权限名称
     */
    @TableField("title")
    private String title;

    /**
     * 权限描述
     */
    @TableField("description")
    private String description;

    /**
     * 权限访问路径
     */
    @TableField("url")
    private String url;

    /**
     * 权限标识
     */
    @TableField("perms")
    private String perms;

    /**
     * 父级权限id
     */
    @TableField("parent_id")
    private String parentId;

    /**
     * 类型   0：目录   1：菜单   2：按钮
     */
    @TableField("type")
    private Integer type;

    /**
     * 排序
     */
    @TableField("order_num")
    private Integer orderNum;

    /**
     * 图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 是否选中
     */
    @TableField(exist = false)
    private boolean checked;

    /**
     * 是否展开
     */
    @TableField(exist = false)
    private boolean spread=true;


    @TableField(exist = false)
    private List<SysPermissionEntity> children;


}
