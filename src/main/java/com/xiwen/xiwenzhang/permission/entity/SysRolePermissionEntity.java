package com.xiwen.xiwenzhang.permission.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author Xiwen
 * @since 2021-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_role_permission")
public class SysRolePermissionEntity implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(type = IdType.UUID)
    private String id;

    /**
     * 权限id
     */
    @TableField("permission_id")
    private String permissionId;

    /**
     * 角色id
     */
    @TableField("role_id")
    private String roleId;


    /**
     * 是否选中
     */
    @TableField("checked")
    private boolean checked;


}
