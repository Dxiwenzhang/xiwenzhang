package com.xiwen.xiwenzhang.permission.Service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiwen.common.utils.MenuTreeUtil;
import com.xiwen.xiwenzhang.permission.Service.SysPermissionService;
import com.xiwen.xiwenzhang.permission.dao.SysPermissionDao;
import com.xiwen.xiwenzhang.permission.entity.SysPermissionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 * @author Xiwen
 * @since 2021-10-29
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionDao, SysPermissionEntity> implements SysPermissionService {

    @Autowired
    private SysPermissionDao sysPermissiondao;

    /**
     * @Dep 根据用户id查找相应的菜单
     * @Author xiwen
     * @Date 2021/10/30 21:31
     */
    @Override
    public List<SysPermissionEntity> findMenuByUserId(String id) {
        List<SysPermissionEntity> menuList= sysPermissiondao.findMenuByUserId(id);
        MenuTreeUtil menuTreeUtil = new MenuTreeUtil();
        return menuTreeUtil.menuList(menuList);
    }

    /**
     * @Dep 菜单管理
     * @Author xiwen
     * @Date 2021/11/7 21:32
     */
    @Override
    public List<SysPermissionEntity> selectPermissionList() {
        List<SysPermissionEntity> menuList= sysPermissiondao.selectPermissionList();
        return menuList;
    }

    @Override
    public List<SysPermissionEntity> selectAll(String roleId) {
        List<SysPermissionEntity> menuList= sysPermissiondao.selectAll(roleId);
        return menuList;
    }

    /**
     * @Dep 添加
     * @Author xiwen
     * @Date 2021/11/14 17:53
     */
    @Override
    public int saveMenu(SysPermissionEntity permission) {
        permission.setId(IdUtil.simpleUUID());
        int insert = sysPermissiondao.insert(permission);
        return insert;
    }

    @Override
    public int selectPermissionById(String id) {
        int count =sysPermissiondao.selectPermissionById(id);
        return count;
    }

    @Override
    public int deleteById(String id) {
        int delete = sysPermissiondao.deleteById(id);
        return delete;
    }

    @Override
    public SysPermissionEntity findPermissionById(String id) {
        SysPermissionEntity sysPermissionEntity = sysPermissiondao.findPermissionById(id);
        return sysPermissionEntity;
    }
}
