package com.xiwen.xiwenzhang.permission.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiwen.xiwenzhang.permission.entity.SysPermissionEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 * @author Xiwen
 * @since 2021-10-29
 */
public interface SysPermissionService extends IService<SysPermissionEntity> {

    List<SysPermissionEntity> findMenuByUserId(String id);

    List<SysPermissionEntity> selectPermissionList();

    List<SysPermissionEntity> selectAll(String roleId);

    int saveMenu(SysPermissionEntity permission);

    int selectPermissionById(String id);

    int deleteById(String id);

    SysPermissionEntity findPermissionById(String id);
}
